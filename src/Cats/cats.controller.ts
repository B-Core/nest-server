import { 
  Controller,
  Get,
  Post,
  Delete,
  Query,
  Body,
  BadRequestException,
} from '@nestjs/common';
import { flatten } from 'lodash/array';
import db from '../db';

import { Cat } from './Cat';

@Controller('cats')
export class CatsController {
  @Get()
  findAll(@Query() query) {
    return db.get('cats')
            .filter(query)
            .value();
  }
  @Post()
  createCat(@Body() body) {
    const cats = db.get('cats');
    let cat;

    try {
      cat = new Cat(body);
    } catch(e) {
      throw new BadRequestException(e.message);
    }

    return cats.push(cat).write();
  }
  @Delete()
  deleteCat(@Body() body) {
    if (!(body instanceof Array) || body.length === 0) return [];
    const deleted = [];
    const cats = db.get('cats');
    body.forEach(query => deleted.push(cats.remove(query).write()));
    return flatten(deleted);
  }
}