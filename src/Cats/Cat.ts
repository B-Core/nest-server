import { v4 } from 'uuid';

export type CatType = 'Furry' | 'Playful' | 'Normal';

export interface Cat {
  id: string;
  name: string;
  type: CatType;
}

export type CatArguments = {
  name: string;
  type?: CatType;
}

const checkErrors = (args, types) => {
  const keys = Object.keys(types);
  for (let i = 0; i < keys.length; i++) {
    if (!types[keys[i]].test(args[keys[i]])) throw new TypeError(types[keys[i]].message);
  }
  return true;
}

export class Cat {
  constructor(args: CatArguments) {

    checkErrors(args, {
      type: {
        test:  v => v === 'Furry' || v === 'Playful' || v === 'Normal' || typeof v === 'undefined',
        message: `Cat creation 'type' is not correct
        Accepted types are 'Furry | Playful | Normal'`,
      },
    });

    this.id = v4();
    this.name = args.name;
    this.type = args.type || 'Normal';
  }
}