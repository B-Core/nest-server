import { Module } from '@nestjs/common';
import { AppController } from './app.controller';

import { CatsModule } from './Cats/cats.module';

@Module({
  imports: [
    CatsModule,
  ],
  controllers: [AppController],
  components: [],
})
export class ApplicationModule {}
